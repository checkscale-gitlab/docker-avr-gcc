FROM ubuntu:18.04

ARG AVR_GCC_VER
ENV AVR_GCC_VER=$AVR_GCC_VER


RUN apt update -q
RUN apt upgrade -y -q
RUN apt install -y -q wget make git tar

RUN wget https://ww1.microchip.com/downloads/en/DeviceDoc/avr8-gnu-toolchain-${AVR_GCC_VER}-linux.any.x86_64.tar.gz -O avr-gcc.tar.gz 
RUN tar -xf "avr-gcc.tar.gz"
RUN rm "avr-gcc.tar.gz"

ENV PATH="/avr8-gnu-toolchain-linux_x86_64/bin:${PATH}"

